from tablet.models import *

from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext

def index(request):
  tablets = Tablet.objects.all()[0:3]
  return render_to_response('tablet/index.html', {'tablets': tablets}, context_instance=RequestContext(request))

def tablet(request, id):
  tablet = get_object_or_404(Tablet, pk=id)
  return render_to_response('tablet/tablet.html', {'tablet': tablet}, context_instance=RequestContext(request))

  
