from django.db import models

class Tablet(models.Model):
  make = models.CharField(max_length=255)
  model = models.CharField(max_length=255)
  height = models.DecimalField(max_digits=9, decimal_places=2)
  width = models.DecimalField(max_digits=9, decimal_places=2)
  depth = models.DecimalField(max_digits=9, decimal_places=2)
  weight = models.DecimalField(max_digits=9, decimal_places=2)

  res_height = models.DecimalField(max_digits=9, decimal_places=2)
  res_width = models.DecimalField(max_digits=9, decimal_places=2)
  screen_size = models.DecimalField(max_digits=9, decimal_places=2) 

  front_img = models.ImageField(upload_to='tablet')
  profile_img = models.ImageField(upload_to='tablet')

  def __unicode__(self):
    return '%s %s' % (self.make, self.model)
