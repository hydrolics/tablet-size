from django.conf import settings

from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Admin
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),

    # Tablet 
    url(r'^tablets/$', 'tablet.views.index'),
    url(r'^tablet/(?P<id>\d+)/$', 'tablet.views.tablet'),
)


if settings.DEBUG:
  urlpatterns += patterns('',
    url(r'^media/(?P<path>.*)', 'django.views.static.serve', { 'document_root': settings.MEDIA_ROOT, })
  )
