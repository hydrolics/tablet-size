  "use strict";

  // Description:
  // Converts a 2d object given in physical size units into its size in pixels.
  // Parameters:
  //  diagonal = the physical size of the diagonal of the user's monitor;
  //  aspectRatio = the physical aspect ratio of the user's monitor;
  //  pixelsX = the horizontal resolution of the user's monitor;
  //  pixelsY = the vertical resolution of the user's monitor.
  //    item = the item to convert, must have .physicalWidth and .physicalHeight
  // Returns:
  //  .width = the width in pixels
  //  .height = the height in pixels
  function convert2d(diagonal, aspectRatio, pixelsX, pixelsY, item) 
  {
    // calculation for height and width is basically the same
    var convert1d = function(realSize, numPixels, aspect) {
      return realSize * numPixels / diagonal * Math.sqrt(aspect * aspect + 1);
    }
  
    return {
      width: convert1d(item.physicalWidth, pixelsX, 1 / aspectRatio),
      height: convert1d(item.physicalHeight, pixelsY, aspectRatio)
    };
  }  
   
  // Description:
  // Holds the monitor data.
  var Monitor = { 
    width: screen.width,
    height: screen.height,
    diagonal: 23,
    aspectRatio: 16 / 9.0
  };
  
  function convert(item)
  {
    // if no aspect provided, assume native res.
    var aspect = (Monitor.aspectRatio == null) ? pixelsX / pixelsY : Monitor.aspectRatio; 
  
    return convert2d(Monitor.diagonal, aspect, Monitor.width, Monitor.height, item);
  }

  // test code
  /**
  var el = document.getElementById('square');
  
  var trect = convert({physicalWidth: 2+11/16.0, physicalHeight: 5.0});

  el.style.width = trect.width;
  el.style.height = trect.height;
  */
$(document).ready(function() {
  var trect = null;
  console.log($('img'));


  var els = $('img');
  for (var i=0, len=els.length; i<len; i++) {
    console.log($(els[i]).attr('width'));
    trect = convert({physicalWidth: 2+11/16.0, physicalHeight: 5.0});
    $(els[i]).attr('width', trect.width)
    $(els[i]).attr('height', trect.height)
  }
  /*$('img').each(function(i) {
    trect = convert({physicalWidth: 2+11/16.0, physicalHeight: 5.0});
    var el = $(this);

    //el['0'].style.width = trect.width;
    //el['0'].style.height = trect.height;

  });
*/



});

